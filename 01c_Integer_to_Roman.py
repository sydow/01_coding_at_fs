def convert_to_roman (integer):
    dict_hundreds = {0: '', 1: 'I', 2: 'II', 3: 'III', 4:'IV', 5: 'V', 6: 'VI', 7: 'VII', 8:'VIII', 9: 'IX', 
    10: 'X', 20: 'XX', 30: 'XXX', 40:'XL', 50: 'L', 60: 'LX', 70: 'LXX', 80:'LXXX', 90: 'XC',
    100: 'C', 200: 'CC', 300: 'CCC', 400:'CD', 500: 'D', 600: 'DC', 700: 'DCC', 800:'DCCC', 900: 'CM'}

    roman = '' # create empty variable for the roman number
    string_integer = str(integer) #  transform integer to type string, otherwise its length cannot be calculated
    length_int = len(string_integer)

    while True: # while loop to exit, if the given integer is not >= 1000
        # add the ones
        number = string_integer[-1]
        roman = dict_hundreds[int(number)] + roman
        if length_int == 1: # if given integer is between 0 and 9, the loop is exited
            return roman
        # add the tenths
        number = string_integer[-2] + '0'
        roman = dict_hundreds[int(number)] + roman
        if length_int == 2: # if given integer is between 10 and 99, the loop is exited
            return roman
        # add the hundreds
        number = string_integer[-3] + '00'
        roman = dict_hundreds[int(number)] + roman
        if length_int == 3:
            return roman # if given integer is between 100 and 999, the loop is exited
        # add the thousands
        number = string_integer[:-3]
        roman = int(number) * 'M' + roman
        return roman

# execute function
print(convert_to_roman(2111))
